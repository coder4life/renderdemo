#pragma once

#include <Windows.h>

namespace renderdemo {

struct EmbeddedData {
   const void* start;
   DWORD size;

   explicit operator bool() const { return start != nullptr; }
};

/// Retrieve a pointer to an RCDATA resource.
/// The returned data is null if the function fails, and the error information
/// can be retrieved by calling GetLastError.
inline auto findEmbeddedData(HMODULE module, LPCTSTR name) -> EmbeddedData
{
   if (const auto resInfo = FindResource(module, name, RT_RCDATA)) {
      if (const auto resData = LoadResource(module, resInfo))
         return { LockResource(resData), SizeofResource(module, resInfo) };
   }
   return {};
}

/// Find data embedded in the executable file for the current process.
inline auto findEmbeddedData(LPCTSTR name) -> EmbeddedData
{
   return findEmbeddedData(/* module */ nullptr, name);
}

} // namespace renderdemo
