#include "pch.h"
#include "DeviceResources3D.h"

#include <d3d11_1.h>
#include <dxgi1_2.h>

#include "ComHelpers.h"

namespace renderdemo {

void DeviceResources3D::createWindowSizeDependentResources()
{
   DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
   VALIDATE_HR(swapChain()->GetDesc1(&swapChainDesc));

   // Create the render-target-view.
   {
      auto backBuffer = ComPtr<ID3D11Texture2D>{};
      VALIDATE_HR(swapChain()->GetBuffer(0, IID_PPV_ARGS(&backBuffer)));

      VALIDATE_HR(
         device()->CreateRenderTargetView(
            backBuffer, nullptr, &m_renderTargetView));
   }

   // Create the depth-stencil-view.
   {
      auto tex = ComPtr<ID3D11Texture2D>{};
      {
         auto texDesc = D3D11_TEXTURE2D_DESC{};
         texDesc.Width = swapChainDesc.Width;
         texDesc.Height = swapChainDesc.Height;
         texDesc.MipLevels = 1;
         texDesc.ArraySize = 1;
         texDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
         texDesc.SampleDesc.Count = 1;
         texDesc.SampleDesc.Quality = 0;
         texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

         VALIDATE_HR(device()->CreateTexture2D(&texDesc, nullptr, &tex));
      }

      VALIDATE_HR(
         device()->CreateDepthStencilView(tex, nullptr, &m_depthStencilView));
   }

   // Setup the viewport.
   m_screenViewport.TopLeftX = 0;
   m_screenViewport.TopLeftY = 0;
   m_screenViewport.Width = static_cast<float>(swapChainDesc.Width);
   m_screenViewport.Height = static_cast<float>(swapChainDesc.Height);
   m_screenViewport.MinDepth = D3D11_MIN_DEPTH;
   m_screenViewport.MaxDepth = D3D11_MAX_DEPTH;
}

void DeviceResources3D::releaseWindowSizeDependentResources()
{
   m_renderTargetView.Release();
   m_depthStencilView.Release();
   deviceContext()->OMSetRenderTargets(0, nullptr, nullptr);
   deviceContext()->Flush();
}

} // namespace renderdemo
