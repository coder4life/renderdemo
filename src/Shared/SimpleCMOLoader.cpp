#include "pch.h"
#include "SimpleCMOLoader.h"

#include <array>
#include <iostream>
#include <type_traits>
#include <vector>

namespace renderdemo {

/*------------------------------------------------------------------------------
   CMO Parsing
   See CMOSpec.h for the layout specification.
------------------------------------------------------------------------------*/

CMOTooComplex::~CMOTooComplex() = default;

namespace {
   class CMOParser {
   public:
      explicit CMOParser(std::istream& stream)
         : m_stream{stream}
         , m_origExceptions{stream.exceptions()}
      {
         // Enable exceptions on the stream to simplify error handling.
         m_stream.exceptions(
            std::ios::badbit | std::ios::failbit | std::ios::eofbit);
      }

      ~CMOParser()
      {
         // Restore the stream to its original exception state.
         m_stream.exceptions(m_origExceptions);
      }

      auto parse() -> CompiledMeshData
      {
         auto data = CompiledMeshData{};

         // Extract the vertex buffer and index buffer.
         expectValue(1u, "MeshCount != 1");
         skipName();
         expectValue(0u, "MaterialCount != 0");
         expectValue('\0', "HasSkeletalAnimationData");
         expectValue(1u, "SubMeshCount != 1");
         m_stream.seekg(sizeof(VSD3DStarter::SubMesh), std::ios::cur);
         expectValue(1u, "IBCount != 1");
         readBuffer(data.indices);
         expectValue(1u, "VBCount != 1");
         readBuffer(data.vertices);
         expectValue(0u, "SkinningVBCount != 0");
         m_stream.seekg(sizeof(VSD3DStarter::MeshExtents), std::ios::cur);

         return data;
      }

   private:
      /// Read a (potentially unaligned) value.
      template <typename T,
         typename = std::enable_if_t<
            std::is_trivial_v<T> && std::is_standard_layout_v<T>>>
      auto readValue() -> T
      {
         T value;
         m_stream.read(reinterpret_cast<char*>(&value), sizeof(value));
         return value;
      }

      /// Read a value and throw is it differs from the expected value.
      /// \param value The expected value to compare with.
      /// \msg Message used in the exception on failure.
      template <typename T>
      void expectValue(T value, const char* msg)
      {
         if (readValue<T>() != value)
            throw CMOTooComplex{msg};
      }

      /// Skip over an encoded name string without reading it.
      void skipName()
      {
         if (const auto nameLength = readValue<UINT>())
            m_stream.seekg(nameLength * sizeof(wchar_t), std::ios::cur);
      }

      /// Read an encoded array into the given buffer.
      template <typename T>
      void readBuffer(std::vector<T>& buffer)
      {
         const auto elementCount = readValue<UINT>();
         const auto dataSize = elementCount * sizeof(T);

         buffer.resize(elementCount);
         m_stream.read(reinterpret_cast<char*>(buffer.data()), dataSize);
      }

      std::istream& m_stream;
      std::ios::iostate m_origExceptions;
   };
}

auto loadMeshFromCMO(std::istream& stream) -> CompiledMeshData
{
   return CMOParser{stream}.parse();
}

/*----------------------------------------------------------------------------*/

namespace {
   /// Create an input element description with sensible default values for most
   /// of the fields.
   constexpr auto makeElementDesc(
      const char* semanticName, DXGI_FORMAT format) -> D3D11_INPUT_ELEMENT_DESC
   {
      return {
         /* SemanticName */ semanticName,
         /* SemanticIndex */ 0,
         /* Format */ format,
         /* InputSlot */ 0,
         /* AlignedByteOffset */ D3D11_APPEND_ALIGNED_ELEMENT,
         /* InputSlotClass */ D3D11_INPUT_PER_VERTEX_DATA,
         /* InstanceDataStepRate */ 0,
      };
   }
}

const std::array<D3D11_INPUT_ELEMENT_DESC, 5>
   CompiledMeshData::Vertex::elementDescs = {
      makeElementDesc("POSITION", DXGI_FORMAT_R32G32B32_FLOAT),
      makeElementDesc("NORMAL", DXGI_FORMAT_R32G32B32_FLOAT),
      makeElementDesc("TANGENT", DXGI_FORMAT_R32G32B32A32_FLOAT),
      makeElementDesc("COLOR", DXGI_FORMAT_R8G8B8A8_UNORM),
      makeElementDesc("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT),
};

} // namespace renderdemo
