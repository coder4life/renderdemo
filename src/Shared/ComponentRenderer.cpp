#include "pch.h"
#include "ComponentRenderer.h"

#include <Windows.h>

#include <CommCtrl.h>
#include <DirectXColors.h>

#include "ComHelpers.h"
#include "DeviceResources3D.h"
#include "StepTimer.h"

namespace renderdemo {

void ComponentRenderer::runAsync(HWND outputWindow)
{
   assert(!isRunning() && "Already running");

   // Kick off the rendering in its own thread.
   m_renderThread = std::thread{
      &ComponentRenderer::renderThreadMain, this, outputWindow};

   // Install our window subclass callback.
   SetWindowSubclass(outputWindow, windowSubclassProc, 0,
      reinterpret_cast<DWORD_PTR>(this));
}

void ComponentRenderer::renderThreadMain(HWND outputWindow)
{
   auto resources = DeviceResources3D{outputWindow};

   for (auto& component : m_components)
      component->onRendererStartup(resources.device());

   for (auto timer = StepTimer{}; true; timer.step()) {
      // Process a message if one is available.
      switch (m_message.exchange(Message::None, std::memory_order_relaxed)) {
      case Message::Resize:
         resources.handleWindowSizeChanged();
         break;

      case Message::ShutDown:
         goto shutdown;
      }

      // Setup the viewport.
      resources.deviceContext()->RSSetViewports(1, &resources.screenViewport());

      // Prepare the render targets.
      {
         const auto renderTargetView = resources.renderTargetView();
         const auto depthStencilView = resources.depthStencilView();

         resources.deviceContext()->ClearRenderTargetView(renderTargetView,
            DirectX::Colors::Blue);

         resources.deviceContext()->ClearDepthStencilView(depthStencilView,
            D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, D3D11_MAX_DEPTH, 0);

         resources.deviceContext()->OMSetRenderTargets(
            1, &renderTargetView, depthStencilView);
      }

      // Render each component.
      for (auto& component : m_components) {
         component->render(
            resources.deviceContext(), timer, resources.screenViewport());
      }

      // Present the rendered image.
      VALIDATE_HR(resources.swapChain()->Present(1, 0));
   }
shutdown:

   for (auto& component : m_components)
      component->onRendererShutdown();
}

auto CALLBACK ComponentRenderer::windowSubclassProc(
   HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam,
   UINT_PTR /* idSubclass */, DWORD_PTR refData) noexcept -> LRESULT
{
   const auto renderer = reinterpret_cast<ComponentRenderer*>(refData);
   switch (msg) {
   case WM_SIZE:
      if (wParam != SIZE_MINIMIZED)
         renderer->m_message.store(Message::Resize, std::memory_order_relaxed);
      break;

   case WM_DESTROY:
      renderer->m_message.store(Message::ShutDown, std::memory_order_relaxed);

      // Wait for the renderer to finish shutting down.
      renderer->m_renderThread.join();

      // We don't need to do anything with the window at this point, so we can
      // remove our subclass callback.
      RemoveWindowSubclass(wnd, windowSubclassProc, 0);

      break;
   }
   return DefSubclassProc(wnd, msg, wParam, lParam);
}

} // namespace renderdemo
