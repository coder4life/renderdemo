#include "pch.h"
#include "MemoryStream.h"

#include <algorithm>
#include <cassert>

namespace renderdemo {

auto MemoryInBuffer::seekoff(
   off_type off, std::ios::seekdir dir, std::ios::openmode which) -> pos_type
{
   if (which & std::ios::in) {
      switch (dir) {
      case std::ios::beg:
         return seekpos(off);

      case std::ios::cur: {
         const auto curPos = pos_type{gptr() - eback()};
         return seekpos(curPos + off);
      }

      case std::ios::end: {
         const auto endPos = pos_type{egptr() - eback()};
         return seekpos(endPos + off);
      }

      default:
         assert(false && "Invalid open mode");
      }
   }
   return std::streampos{-1};
}

auto MemoryInBuffer::seekpos(
   pos_type pos, std::ios::openmode which) -> pos_type
{
   if (!(which & std::ios::in))
      return std::streampos{-1};

   // Clamp the position value between 0 and the total data size.
   pos = std::clamp(static_cast<off_type>(pos),
      static_cast<off_type>(0),
      static_cast<off_type>(egptr() - eback()));

   // Update the get area pointers.
   setg(eback(), eback() + static_cast<std::ptrdiff_t>(pos), egptr());

   return pos;
}

MemoryInStream::~MemoryInStream() = default;

}
