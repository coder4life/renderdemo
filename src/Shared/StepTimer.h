#pragma once

#include <Windows.h>

namespace renderdemo {

// Define performance counter functions that return the value directly, skipping
// the error reporting mechanism; their documentation says that they never fail.
namespace detail {
   // QueryPerformanceFrequency
   inline auto queryPerfFrequency() -> LONGLONG
   {
      LARGE_INTEGER result;
      ::QueryPerformanceFrequency(&result);
      return result.QuadPart;
   }

   // QueryPerformanceCounter
   inline auto queryPerfCounter() -> LONGLONG
   {
      LARGE_INTEGER result;
      ::QueryPerformanceCounter(&result);
      return result.QuadPart;
   }
}

/// Provides high-resolution measurements of the time interval between steps,
/// which are user-defined moments in time (e.g. frames).
class StepTimer {
public:
   StepTimer()
      : m_deltaTime{0.0}
      , m_totalTime{0.0}
      , m_tickDuration{1.0 / detail::queryPerfFrequency()}
      , m_lastTickCount{detail::queryPerfCounter()}
      , m_totalTicks{0}
   {
   }

   auto deltaTime() const -> double { return m_deltaTime; }
   auto totalTime() const -> double { return m_totalTime; }

   /// Advance the timer by one step.
   void step()
   {
      const auto currentTickCount = detail::queryPerfCounter();
      const auto tickDelta = currentTickCount - m_lastTickCount;

      // Update tick-based state.
      m_lastTickCount = currentTickCount;
      m_totalTicks += tickDelta;

      // Recalculate time-based state.
      m_deltaTime = tickDelta * m_tickDuration;
      m_totalTime = m_totalTicks * m_tickDuration;
   }

private:
   /// Seconds since the previous step.
   double m_deltaTime;

   /// Seconds since the timer creation.
   double m_totalTime;

   /// Seconds per tick of the performance counter.
   /// In other words, this is the reciprocal of the counter frequency.
   const double m_tickDuration;

   /// Value of the performance counter at the last step.
   LONGLONG m_lastTickCount;

   /// Number of performance counter ticks since the timer was created.
   /// This represents the same value as m_totalTime, but it's maintained
   /// independently to avoid floating point error accumulation.
   LONGLONG m_totalTicks;
};

} // namespace renderdemo
