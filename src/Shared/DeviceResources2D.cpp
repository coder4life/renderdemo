#include "pch.h"
#include "DeviceResources2D.h"

#include <d2d1_1.h>

#include "ComHelpers.h"

namespace renderdemo {

void DeviceResources2D::createDeviceIndependentResources()
{
   // Create the Direct2D factory.
   {
      auto options = D2D1_FACTORY_OPTIONS{};
#ifdef _DEBUG
      options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
#endif

      VALIDATE_HR(
         D2D1CreateFactory(
            D2D1_FACTORY_TYPE_SINGLE_THREADED, options, &m_factory));
   }
}

void DeviceResources2D::createDeviceDependentResources()
{
   // Cast the Direct3D device to a DXGI device.
   auto dxgiDevice = ComPtr<IDXGIDevice>{};
   VALIDATE_HR(d3dDevice()->QueryInterface(IID_PPV_ARGS(&dxgiDevice)));

   // Create the Direct2D device.
   VALIDATE_HR(
      m_factory->CreateDevice(dxgiDevice, &m_device));

   // Create the Direct2D device context.
   VALIDATE_HR(
      m_device->CreateDeviceContext(
         D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &m_deviceContext));
}

void DeviceResources2D::createWindowSizeDependentResources()
{
   auto backBuffer = ComPtr<IDXGISurface>{};
   VALIDATE_HR(swapChain()->GetBuffer(0, IID_PPV_ARGS(&backBuffer)));

   // Create a targetable bitmap from the back buffer.
   {
      auto props = D2D1_BITMAP_PROPERTIES1{};
      props.pixelFormat.format = DXGI_FORMAT_B8G8R8A8_UNORM;
      props.pixelFormat.alphaMode = D2D1_ALPHA_MODE_IGNORE;
      props.bitmapOptions
         = D2D1_BITMAP_OPTIONS_TARGET
         | D2D1_BITMAP_OPTIONS_CANNOT_DRAW;

      VALIDATE_HR(
         m_deviceContext->CreateBitmapFromDxgiSurface(
            backBuffer, props, &m_backBufferBitmap));
   }
}

void DeviceResources2D::releaseWindowSizeDependentResources()
{
   m_backBufferBitmap.Release();
   m_deviceContext->SetTarget(nullptr);
}

} // namespace renderdemo
