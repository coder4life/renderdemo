// Stars_PS.hlsl
// Generates a starfield from a texture containing pre-computed noise values.

Texture2D noiseTexture;
SamplerState noiseSampler;

/// Minimum value of the noise function to interpret as a star.
/// Lower values result in more stars being displayed.
static const float POINT_THRESHOLD = 0.95f;

float4 main(float2 texCoord : TEXCOORD) : SV_TARGET
{
   float value = noiseTexture.Sample(noiseSampler, texCoord).r;

   // Discard values that are below the threshold.
   value *= step(POINT_THRESHOLD, value);

   return float4(value, value, value, 1);
}
