#include "pch.h"

#include <Windows.h>
#include <windowsx.h>

#include <Shared/ComponentRenderer.h>

#include "BackdropComponent.h"
#include "ContentComponent.h"

using namespace renderdemo;
using namespace renderdemo::holographic;

namespace {

struct WindowData {
   ComponentRenderer renderer;
};

auto getWindowData(HWND wnd) -> WindowData*
{
   const auto lp = GetWindowLongPtr(wnd, GWLP_USERDATA);
   return reinterpret_cast<WindowData*>(lp);
}

void setWindowData(HWND wnd, WindowData* data)
{
   const auto lp = reinterpret_cast<LONG_PTR>(data);
   SetWindowLongPtr(wnd, GWLP_USERDATA, lp);
}

/*------------------------------------------------------------------------------
   Message Handlers
------------------------------------------------------------------------------*/

auto MainWindow_onCreate(HWND wnd, LPCREATESTRUCT) -> BOOL
{
   const auto data = new WindowData;
   setWindowData(wnd, data);

   data->renderer.addComponent(std::make_unique<BackdropComponent>());
   data->renderer.addComponent(std::make_unique<ContentComponent>());
   data->renderer.runAsync(wnd);

   return TRUE;
}

void MainWindow_onDestroy(HWND wnd)
{
   delete getWindowData(wnd);
   PostQuitMessage(0);
}

/*----------------------------------------------------------------------------*/

auto CALLBACK windowProc(
   HWND wnd, UINT msg,
   WPARAM wParam, LPARAM lParam) noexcept -> LRESULT
{
   switch (msg) {
      HANDLE_MSG(wnd, WM_CREATE, MainWindow_onCreate);
      HANDLE_MSG(wnd, WM_DESTROY, MainWindow_onDestroy);

   default:
      return DefWindowProc(wnd, msg, wParam, lParam);
   }
}

auto createMainWindow(HINSTANCE instance) -> HWND
{
   static const auto atom = [instance] {
      auto wc = WNDCLASSEX{sizeof(WNDCLASSEX)};
      wc.style = CS_VREDRAW | CS_HREDRAW;
      wc.lpfnWndProc = windowProc;
      wc.hInstance = instance;
      wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
      wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
      wc.lpszClassName = L"Holographic_MainWindow";

      return RegisterClassEx(&wc);
   }();

   return CreateWindowEx(
      /* dwExStyle */ WS_EX_CLIENTEDGE,
      /* lpClassName */ MAKEINTATOM(atom),
      /* lpWindowName */ L"Holographic",
      /* dwStyle */ WS_OVERLAPPEDWINDOW,
      /* X */ CW_USEDEFAULT,
      /* Y */ CW_USEDEFAULT,
      /* nWidth */ CW_USEDEFAULT,
      /* nHeight */ CW_USEDEFAULT,
      /* hWndParent */ nullptr,
      /* hMenu */ nullptr,
      /* hInstance */ instance,
      /* lpParam */ nullptr);
}

} // namespace

auto APIENTRY wWinMain(
   _In_ HINSTANCE instance, _In_opt_ HINSTANCE /* prevInstance */,
   _In_ LPWSTR /* cmdLine */, _In_ int cmdShow) -> int
{
   const auto mainWindow = createMainWindow(instance);
   if (!mainWindow) {
      MessageBox(nullptr,
         L"Failed to create the main window", L"Error", MB_ICONERROR);
      return 0;
   }

   ShowWindow(mainWindow, cmdShow);
   UpdateWindow(mainWindow);

   for (MSG msg;;) {
      switch (GetMessage(&msg, nullptr, 0, 0)) {
      case -1:
         // Call failed.
         return 0;

      case 0:
         // Received WM_QUIT.
         return static_cast<int>(msg.wParam);

      default:
         TranslateMessage(&msg);
         DispatchMessage(&msg);
      }
   }
}
