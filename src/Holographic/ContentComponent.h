#pragma once

#include <d3d11_1.h>

#include <Shared/ComHelpers.h>
#include <Shared/ComponentRenderer.h>

namespace renderdemo::holographic {

class ContentComponent : public IRenderComponent {
public:
   void onRendererStartup(ID3D11Device1*) override;

   void render(ID3D11DeviceContext1*,
      const StepTimer&, const D3D11_VIEWPORT& screenViewport) override;

   void onRendererShutdown() override;

private:
   void updateScene(ID3D11DeviceContext1*,
      const StepTimer&, const D3D11_VIEWPORT& screenViewport);

   void configureInputAssembler(ID3D11DeviceContext1*);
   void setShaders(ID3D11DeviceContext1*);
   void bindConstantBuffers(ID3D11DeviceContext1*);

   ComPtr<ID3D11Buffer> m_vertexBuffer;
   ComPtr<ID3D11Buffer> m_indexBuffer;
   ComPtr<ID3D11InputLayout> m_inputLayout;
   UINT m_indexCount = 0;

   ComPtr<ID3D11VertexShader> m_vertexShader;
   ComPtr<ID3D11PixelShader> m_pixelShader;

   ComPtr<ID3D11Buffer> m_vsPerFrame;
   ComPtr<ID3D11Buffer> m_vsPerObject;
   ComPtr<ID3D11Buffer> m_psPerFrame;

   ComPtr<ID3D11BlendState> m_blendState;
};

} // namespace renderdemo::holographic
