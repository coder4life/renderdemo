#include "pch.h"
#include "ContentComponent.h"

#include <DirectXMath.h>

#include <Shared/EmbeddedData.h>
#include <Shared/MemoryStream.h>
#include <Shared/SimpleCMOLoader.h>
#include <Shared/StepTimer.h>

#include "Resource.h"

using namespace DirectX;

namespace renderdemo::holographic {

namespace {
   /*---------------------------------------------------------------------------
      Constants
   ---------------------------------------------------------------------------*/

   /// Rotation speed of the model, in radians-per-second.
   constexpr auto kRotationSpeed = 0.3f;

   /// Position of the camera, in world space.
   constexpr auto kCameraPosition = XMFLOAT3{0.0f, 0.0f, -2.5f};

   /// Transformation from world space to camera space.
   const auto kViewMatrix = XMMatrixLookAtLH(
      /* EyePosition */ XMLoadFloat3(&kCameraPosition),
      /* FocusPosition */ XMVectorZero(),
      /* UpDirection */ XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));

   /// Field-of-view angle, in radians.
   constexpr auto kFOV = XM_PIDIV4;

   /// Distance to the near clipping plane.
   constexpr auto kNearZ = 1.0f;

   /// Distance to the far clipping plane.
   constexpr auto kFarZ = 1000.0f;

   /*---------------------------------------------------------------------------
      Shader Constant Buffers
   ---------------------------------------------------------------------------*/

   struct VSPerFrame {
      XMMATRIX viewProjection;
      float time;
   };

   struct VSPerObject {
      XMMATRIX world;
   };

   struct PSPerFrame {
      XMFLOAT3 cameraPos;
      float time;
   };

   /*-------------------------------------------------------------------------*/

   auto loadEmbeddedMesh(LPCTSTR name) -> CompiledMeshData
   {
      const auto [data, dataLen] = findEmbeddedData(name);
      auto stream = MemoryInStream{data, dataLen};
      return loadMeshFromCMO(stream);
   }

   template <typename T>
   auto createMeshBuffer(
      ID3D11Device* device,
      const std::vector<T>& elems,
      UINT bindFlags,
      ID3D11Buffer** outBuffer) -> HRESULT
   {
      auto desc = D3D11_BUFFER_DESC{};
      desc.ByteWidth = static_cast<UINT>(elems.size() * sizeof(T));
      desc.Usage = D3D11_USAGE_IMMUTABLE;
      desc.BindFlags = bindFlags;

      auto resource = D3D11_SUBRESOURCE_DATA{};
      resource.pSysMem = elems.data();

      return device->CreateBuffer(&desc, &resource, outBuffer);
   }

   auto createMeshInputLayout(
      ID3D11Device* device,
      const void* vsData,
      std::size_t vsLen,
      ID3D11InputLayout** outLayout) -> HRESULT
   {
      const auto& descs = CompiledMeshData::Vertex::elementDescs;

      return device->CreateInputLayout(
         descs.data(),
         static_cast<UINT>(descs.size()),
         vsData,
         vsLen,
         outLayout);
   }

   template <typename T>
   auto createConstantBuffer(
      ID3D11Device* device, ID3D11Buffer** outBuffer) -> HRESULT
   {
      static_assert((sizeof(T) & 15) == 0, "Invalid constant buffer size");

      auto desc = D3D11_BUFFER_DESC{};
      desc.ByteWidth = sizeof(T);
      desc.Usage = D3D11_USAGE_DEFAULT;
      desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

      return device->CreateBuffer(&desc, nullptr, outBuffer);
   }

   auto createAlphaBlendState(
      ID3D11Device* device, ID3D11BlendState** outState) -> HRESULT
   {
      // Blend colours using the source alpha.
      auto desc = D3D11_BLEND_DESC{};
      desc.RenderTarget[0].BlendEnable = true;
      desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
      desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
      desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
      desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
      desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
      desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
      desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

      return device->CreateBlendState(&desc, outState);
   }
}

void ContentComponent::onRendererStartup(ID3D11Device1* device)
{
   const auto mesh = loadEmbeddedMesh(MAKEINTRESOURCE(IDR_TEAPOT_CMO));

   const auto [vsData, vsLen]
      = findEmbeddedData(MAKEINTRESOURCE(IDR_HOLOGRAM_VS));
   const auto [psData, psLen]
      = findEmbeddedData(MAKEINTRESOURCE(IDR_HOLOGRAM_PS));

   // Create mesh-related objects.
   VALIDATE_HR(
      createMeshBuffer(device,
         mesh.vertices, D3D11_BIND_VERTEX_BUFFER, &m_vertexBuffer));
   VALIDATE_HR(
      createMeshBuffer(device,
         mesh.indices, D3D11_BIND_INDEX_BUFFER, &m_indexBuffer));
   VALIDATE_HR(
      createMeshInputLayout(device, vsData, vsLen, &m_inputLayout));

   // Save the number of indices in the mesh so we know what to draw.
   m_indexCount = static_cast<UINT>(mesh.indices.size());

   // Create shaders.
   VALIDATE_HR(
      device->CreateVertexShader(vsData, vsLen, nullptr, &m_vertexShader));
   VALIDATE_HR(
      device->CreatePixelShader(psData, psLen, nullptr, &m_pixelShader));

   // Create constant buffers.
   VALIDATE_HR(createConstantBuffer<VSPerFrame>(device, &m_vsPerFrame));
   VALIDATE_HR(createConstantBuffer<VSPerObject>(device, &m_vsPerObject));
   VALIDATE_HR(createConstantBuffer<PSPerFrame>(device, &m_psPerFrame));

   // Create the blend state.
   VALIDATE_HR(createAlphaBlendState(device, &m_blendState));
}

void ContentComponent::updateScene(
   ID3D11DeviceContext1* deviceCtx,
   const StepTimer& timer, const D3D11_VIEWPORT& screenViewport)
{
   const auto aspectRatio
      = static_cast<float>(screenViewport.Width)
      / static_cast<float>(screenViewport.Height);

   const auto projectionMatrix = XMMatrixPerspectiveFovLH(
      kFOV, aspectRatio, kNearZ, kFarZ);

   // Rotate the model around the Y-axis.
   const auto worldMatrix = XMMatrixRotationY(
      static_cast<float>(timer.totalTime()) * kRotationSpeed);

   // Update VSPerFrame.
   {
      auto buffer = VSPerFrame{};
      buffer.viewProjection = XMMatrixTranspose(kViewMatrix * projectionMatrix);
      buffer.time = static_cast<float>(timer.totalTime());

      deviceCtx->UpdateSubresource(m_vsPerFrame, 0, nullptr, &buffer, 0, 0);
   }

   // Update VSPerObject.
   {
      auto buffer = VSPerObject{};
      buffer.world = XMMatrixTranspose(worldMatrix);

      deviceCtx->UpdateSubresource(m_vsPerObject, 0, nullptr, &buffer, 0, 0);
   }

   // Update PSPerFrame.
   {
      auto buffer = PSPerFrame{};
      buffer.cameraPos = kCameraPosition;
      buffer.time = static_cast<float>(timer.totalTime());

      deviceCtx->UpdateSubresource(m_psPerFrame, 0, nullptr, &buffer, 0, 0);
   }
}

void ContentComponent::configureInputAssembler(ID3D11DeviceContext1* deviceCtx)
{
   const auto vb = m_vertexBuffer.GetInterfacePtr();
   constexpr UINT vbStride = sizeof(CompiledMeshData::Vertex);
   constexpr UINT vbOffset = 0u;

   deviceCtx->IASetVertexBuffers(0, 1, &vb, &vbStride, &vbOffset);
   deviceCtx->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);
   deviceCtx->IASetInputLayout(m_inputLayout);
   deviceCtx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void ContentComponent::setShaders(ID3D11DeviceContext1* deviceCtx)
{
   deviceCtx->VSSetShader(m_vertexShader, nullptr, 0);
   deviceCtx->PSSetShader(m_pixelShader, nullptr, 0);
}

void ContentComponent::bindConstantBuffers(ID3D11DeviceContext1* deviceCtx)
{
   ID3D11Buffer* vsBuffers[] = {m_vsPerFrame, m_vsPerObject};

   deviceCtx->VSSetConstantBuffers(0, ARRAYSIZE(vsBuffers), vsBuffers);
   deviceCtx->PSSetConstantBuffers(0, 1, &m_psPerFrame.GetInterfacePtr());
}

void ContentComponent::render(
   ID3D11DeviceContext1* deviceCtx,
   const StepTimer& timer, const D3D11_VIEWPORT& screenViewport)
{
   updateScene(deviceCtx, timer, screenViewport);

   configureInputAssembler(deviceCtx);
   setShaders(deviceCtx);
   bindConstantBuffers(deviceCtx);

   deviceCtx->OMSetBlendState(m_blendState, nullptr, 0xFFFFFFFF);
   deviceCtx->DrawIndexed(m_indexCount, 0, 0);
   deviceCtx->OMSetBlendState(nullptr, nullptr, 0xFFFFFFFF);
}

void ContentComponent::onRendererShutdown()
{
   m_blendState.Release();
   m_psPerFrame.Release();
   m_vsPerFrame.Release();
   m_vsPerObject.Release();
   m_pixelShader.Release();
   m_vertexShader.Release();
   m_inputLayout.Release();
   m_indexBuffer.Release();
   m_vertexBuffer.Release();
}

} // namespace renderdemo::holographic
