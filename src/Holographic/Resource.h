#pragma once

// Resources
#define IDR_NOISE_DDS 101
#define IDR_TEAPOT_CMO 102

// Shaders
#define IDR_BACKDROP_VS 1001
#define IDR_STARS_PS 1002
#define IDR_HOLOGRAM_VS 1003
#define IDR_HOLOGRAM_PS 1004
