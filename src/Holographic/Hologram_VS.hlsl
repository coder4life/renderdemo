cbuffer cbPerFrame : register(b0) {
   float4x4 viewProjection;
   float time;
};

cbuffer cbPerObject : register(b1) {
   float4x4 world;
};

/*------------------------------------------------------------------------------
   Glitching
------------------------------------------------------------------------------*/

static const float glitchActivationFrequency = 0.8f;
static const float glitchActivationThreshold = 0.998f;

static const float glitchMaxAmplitude = 0.15f;
static const float glitchTiling = 75.0f;
static const float glitchWobbleSpeed = 4.0f;

float4 glitch(float4 pos)
{
   const float gate = step(glitchActivationThreshold,
      sin(time * glitchActivationFrequency));

   pos.x += gate
      * glitchMaxAmplitude
      * sin(glitchTiling * pos.y + glitchWobbleSpeed * time);

   return pos;
}

/*----------------------------------------------------------------------------*/

struct VertexShaderInput {
   float4 position : POSITION;
   float3 normal : NORMAL;
};

struct VertexShaderOutput {
   float4 posWorld : POSITION;
   float3 normalWorld : NORMAL;
   float4 position : SV_POSITION;
};

VertexShaderOutput main(VertexShaderInput input)
{
   VertexShaderOutput output;
   output.posWorld = mul(input.position, world);
   output.normalWorld = mul(float4(input.normal, 0), world).xyz;
   output.position = mul(glitch(output.posWorld), viewProjection);
   return output;
}
