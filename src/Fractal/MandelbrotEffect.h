#pragma once

#include <d2d1_1.h>

namespace renderdemo::fractal {

extern "C" const CLSID CLSID_MandelbrotEffect;
extern "C" const GUID GUID_MandelbrotPixelShader;

enum MandelbrotParam {
   /// Size of the output rectangle to render to.
   /// Type: float2
   MandelbrotParam_OutputSize,

   /// Value of the complex number to center the image on.
   /// Type: double2
   MandelbrotParam_Center,

   /// Width of the displayed subset of the complex plane.
   /// Type: double
   MandelbrotParam_Scale,
};

/// Register the effect with the given factory instance.
auto registerMandelbrotEffect(ID2D1Factory1*) -> HRESULT;

} // namespace renderdemo::fractal
