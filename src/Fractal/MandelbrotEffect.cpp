#include "pch.h"
#include "MandelbrotEffect.h"

#include <d2d1_1.h>
#include <d2d1effectauthor.h>
#include <d2d1effecthelpers.h>

#include <cassert>
#include <complex>
#include <new>

#include <Shared/ComHelpers.h>
#include <Shared/EmbeddedData.h>

#include "Resource.h"

namespace renderdemo::fractal {

const CLSID CLSID_MandelbrotEffect = {
   /* 4904ad29-214b-4bef-bafe-2209f61a24ca */
   0x4904ad29,
   0x214b,
   0x4bef,
   {0xba, 0xfe, 0x22, 0x09, 0xf6, 0x1a, 0x24, 0xca}};

const GUID GUID_MandelbrotPixelShader = {
   /* 5f2cafdc-a449-4f9b-8e6d-f2ba9ae332f2 */
   0x5f2cafdc,
   0xa449,
   0x4f9b,
   {0x8e, 0x6d, 0xf2, 0xba, 0x9a, 0xe3, 0x32, 0xf2}};

namespace {
   /// Effect that renders with the Mandelbrot pixel shader.
   /// Since the effect's transform graph consists of just a single node, this
   /// class also implements the interface for that node.
   class MandelbrotEffect : public ID2D1EffectImpl, public ID2D1DrawTransform {
   public:
      MandelbrotEffect() = default;

      auto outputSize() const -> D2D_VECTOR_2F
      {
         return m_params.outputSize;
      }

      auto setOutputSize(D2D_VECTOR_2F size) -> HRESULT
      {
         if (size.x <= 0 || size.y <= 0)
            return E_INVALIDARG;
         m_params.outputSize = size;
         return S_OK;
      }

      auto center() const -> std::complex<double>
      {
         return m_params.center;
      }

      auto setCenter(std::complex<double> center) -> HRESULT
      {
         m_params.center = center;
         return S_OK;
      }

      auto scale() const -> double
      {
         return m_params.scale;
      }

      auto setScale(double scale) -> HRESULT
      {
         if (scale <= 0)
            return E_INVALIDARG;
         m_params.scale = scale;
         return S_OK;
      }

      /*------------------------------------------------------------------------
         ID2D1EffectImpl
      ------------------------------------------------------------------------*/

      auto STDMETHODCALLTYPE Initialize(
         ID2D1EffectContext* effectContext,
         ID2D1TransformGraph* transformGraph) -> HRESULT override
      {
         HRESULT hr;

         // Get the pixel shader bytecode.
         const auto psData = findEmbeddedData(
            MAKEINTRESOURCE(IDR_MANDELBROT_PS));
         if (!psData) {
            hr = HRESULT_FROM_WIN32(GetLastError());
            assert(FAILED(hr) && "Null data but the function succeeded?");
            goto end;
         }

         // Load the shader.
         hr = effectContext->LoadPixelShader(GUID_MandelbrotPixelShader,
            static_cast<const BYTE*>(psData.start), psData.size);
         if (FAILED(hr))
            goto end;

         // Initialise the transform graph.
         hr = transformGraph->SetSingleTransformNode(this);
         if (FAILED(hr))
            goto end;

      end:
         return hr;
      }

      auto STDMETHODCALLTYPE PrepareForRender(
         D2D1_CHANGE_TYPE) -> HRESULT override
      {
         // Update constants.
         return m_drawInfo->SetPixelShaderConstantBuffer(
            reinterpret_cast<const BYTE*>(&m_params), sizeof(m_params));
      }

      auto STDMETHODCALLTYPE SetGraph(
         ID2D1TransformGraph*) -> HRESULT override
      {
         // The effect input count is constant, so this shouldn't be called.
         return E_NOTIMPL;
      }

      /*------------------------------------------------------------------------
         ID2D1DrawTransform
      ------------------------------------------------------------------------*/

      auto STDMETHODCALLTYPE SetDrawInfo(
         ID2D1DrawInfo* drawInfo) -> HRESULT override
      {
         m_drawInfo = drawInfo;
         return drawInfo->SetPixelShader(GUID_MandelbrotPixelShader);
      }

      /*------------------------------------------------------------------------
         ID2D1Transform
      ------------------------------------------------------------------------*/

      auto STDMETHODCALLTYPE MapInputRectsToOutputRect(
         const D2D1_RECT_L* /* inputRects */,
         const D2D1_RECT_L* /* inputOpaqueSubRects */,
         UINT32 inputRectCount,
         D2D1_RECT_L* outputRect,
         D2D1_RECT_L* outputOpaqueSubRect) -> HRESULT override
      {
         if (inputRectCount != 0)
            return E_INVALIDARG;

         const auto width = static_cast<LONG>(m_params.outputSize.x);
         const auto height = static_cast<LONG>(m_params.outputSize.y);

         // The entire output is opaque.
         outputRect->left = outputOpaqueSubRect->left = 0;
         outputRect->top = outputOpaqueSubRect->top = 0;
         outputRect->right = outputOpaqueSubRect->right = width;
         outputRect->bottom = outputOpaqueSubRect->bottom = height;

         return S_OK;
      }

      auto STDMETHODCALLTYPE MapInvalidRect(
         UINT32 /* inputIndex */,
         D2D1_RECT_L /* invalidInputRect */,
         D2D1_RECT_L* /* invalidOutputRect */) const -> HRESULT override
      {
         // The effect doesn't have any inputs, so this shouldn't be called.
         return E_NOTIMPL;
      }

      auto STDMETHODCALLTYPE MapOutputRectToInputRects(
         const D2D1_RECT_L* /* outputRect */,
         D2D1_RECT_L* /* inputRects */,
         UINT32 inputRectsCount) const -> HRESULT override
      {
         // There shouldn't be any input rectangles to map to.
         return (inputRectsCount == 0) ? S_OK : E_INVALIDARG;
      }

      /*------------------------------------------------------------------------
         ID2D1TransformNode
      ------------------------------------------------------------------------*/

      auto STDMETHODCALLTYPE GetInputCount() const -> UINT32 override
      {
         // The pixels are generated entirely in the shader.
         return 0;
      }

      /*------------------------------------------------------------------------
         IUnknown
      ------------------------------------------------------------------------*/

      auto STDMETHODCALLTYPE AddRef() -> ULONG override
      {
         return InterlockedIncrement(&m_refCount);
      }

      auto STDMETHODCALLTYPE Release() -> ULONG override
      {
         const auto count = InterlockedDecrement(&m_refCount);
         if (count == 0)
            delete this;
         return count;
      }

      auto STDMETHODCALLTYPE QueryInterface(
         const IID& iid, void** outObj) -> HRESULT override
      {
         if (!outObj)
            return E_POINTER;

         if (iid == IID_ID2D1EffectImpl)
            *outObj = static_cast<ID2D1EffectImpl*>(this);
         else if (iid == IID_ID2D1DrawTransform)
            *outObj = static_cast<ID2D1DrawTransform*>(this);
         else if (iid == IID_ID2D1Transform)
            *outObj = static_cast<ID2D1Transform*>(this);
         else if (iid == IID_ID2D1TransformNode)
            *outObj = static_cast<ID2D1TransformNode*>(this);
         else if (iid == IID_IUnknown)
            *outObj = this;
         else {
            *outObj = nullptr;
            return E_NOINTERFACE;
         }

         AddRef();

         return S_OK;
      }

      /*----------------------------------------------------------------------*/

   private:
      struct {
         D2D_VECTOR_2F outputSize = {1024, 768};
         double scale = 2;
         std::complex<double> center = {0, 0};
      } m_params;
      ComPtr<ID2D1DrawInfo> m_drawInfo;
      LONG m_refCount = 1;
   };

   /// Callback used by the factory for effect impl creation.
   auto CALLBACK createImpl(IUnknown** obj) noexcept -> HRESULT
   {
      if (!obj)
         return E_POINTER;

      const auto impl = new (std::nothrow) MandelbrotEffect{};
      return (*obj = static_cast<ID2D1EffectImpl*>(impl)) != nullptr
         ? S_OK
         : E_OUTOFMEMORY;
   }
}

auto registerMandelbrotEffect(ID2D1Factory1* factory) -> HRESULT
{
   constexpr auto propertyXml = LR"xml(<?xml version="1.0"?>
<Effect>
   <Property name="DisplayName" type="string" value="Mandelbrot" />
   <Property name="Author" type="string" value="Hamza Sood" />
   <Property name="Category" type="string" value="Fractal" />
   <Property name="Description" type="string" value="Visualisation of the Mandelbrot set" />

   <Inputs />

   <Property name="OutputSize" type="vector2">
      <Property name="DisplayName" type="string" value="Output Size" />
   </Property>

   <Property name="Center" type="blob">
      <Property name="DisplayName" type="string" value="Center" />
   </Property>

   <Property name="Scale" type="blob">
      <Property name="DisplayName" type="string" value="Scale" />
   </Property>
</Effect>
)xml";

   const D2D1_PROPERTY_BINDING bindings[] = {
      D2D1_VALUE_TYPE_BINDING(
         L"OutputSize",
         &MandelbrotEffect::setOutputSize, &MandelbrotEffect::outputSize),

      D2D1_VALUE_TYPE_BINDING(
         L"Center",
         &MandelbrotEffect::setCenter, &MandelbrotEffect::center),

      D2D1_VALUE_TYPE_BINDING(
         L"Scale",
         &MandelbrotEffect::setScale, &MandelbrotEffect::scale),
   };

   return factory->RegisterEffectFromString(CLSID_MandelbrotEffect,
      propertyXml, bindings, ARRAYSIZE(bindings), createImpl);
}

} // namespace renderdemo::fractal
