#include "pch.h"

#include <Windows.h>
#include <windowsx.h>

#include <d2d1_1.h>

#include <complex>

#include <Shared/ComHelpers.h>
#include <Shared/DeviceResources2D.h>

#include "MandelbrotEffect.h"

using namespace renderdemo;
using namespace renderdemo::fractal;

namespace {

/// Amount to adjust the scale by when zooming.
/// Higher values mean more zooming.
constexpr auto kZoomSpeed = 0.05;

struct WindowData {
   explicit WindowData(HWND wnd)
      : deviceResources{wnd}
   {
   }

   DeviceResources2D deviceResources;
   ComPtr<ID2D1Effect> mandelbrotEffect;
   int prevMouseX = 0;
   int prevMouseY = 0;
   bool isScrolling = false;
};

auto getWindowData(HWND wnd) -> WindowData*
{
   const auto lp = GetWindowLongPtr(wnd, GWLP_USERDATA);
   return reinterpret_cast<WindowData*>(lp);
}

void setWindowData(HWND wnd, WindowData* data)
{
   const auto lp = reinterpret_cast<LONG_PTR>(data);
   SetWindowLongPtr(wnd, GWLP_USERDATA, lp);
}

/*------------------------------------------------------------------------------
   Message Handlers
------------------------------------------------------------------------------*/

auto MainWindow_onCreate(HWND wnd, LPCREATESTRUCT) -> BOOL
{
   const auto data = new WindowData{wnd};
   setWindowData(wnd, data);

   // Create the effect.
   {
      const auto& dr = data->deviceResources;

      VALIDATE_HR(registerMandelbrotEffect(dr.factory()));

      VALIDATE_HR(
         dr.deviceContext()->CreateEffect(
            CLSID_MandelbrotEffect, &data->mandelbrotEffect));
   }

   return TRUE;
}

void MainWindow_onPaint(HWND wnd)
{
   const auto data = getWindowData(wnd);
   {
      PAINTSTRUCT ps;
      BeginPaint(wnd, &ps);

      // Draw.
      {
         const auto dc = data->deviceResources.deviceContext();

         dc->BeginDraw();
         dc->SetTarget(data->deviceResources.backBufferBitmap());

         dc->Clear(D2D1::ColorF{D2D1::ColorF::Blue});
         dc->DrawImage(data->mandelbrotEffect);

         VALIDATE_HR(dc->EndDraw());
      }

      // Present.
      VALIDATE_HR(data->deviceResources.swapChain()->Present(1, 0));

      EndPaint(wnd, &ps);
   }
}

void MainWindow_onSize(HWND wnd, UINT state, int cx, int cy)
{
   if (state == SIZE_MINIMIZED)
      return;

   const auto data = getWindowData(wnd);

   // Resize device resources.
   data->deviceResources.handleWindowSizeChanged();

   // Pass the new size to the effect instance.
   VALIDATE_HR(
      data->mandelbrotEffect->SetValue(MandelbrotParam_OutputSize,
         D2D_VECTOR_2F{static_cast<float>(cx), static_cast<float>(cy)}));
}

void MainWindow_onMouseMove(HWND wnd, int x, int y, UINT keyFlags)
{
   const auto data = getWindowData(wnd);

   if (!(keyFlags & MK_LBUTTON)) {
      // We only want to scroll while the left mouse button is down, so we can
      // ignore all other kinds of mouse move events.
      data->isScrolling = false;
      return;
   }

   if (data->isScrolling) {
      const auto effect = data->mandelbrotEffect.GetInterfacePtr();
      const auto scale = effect->GetValue<double>(MandelbrotParam_Scale);

      RECT wndRect;
      GetWindowRect(wnd, &wndRect);

      // Calculate the movement delta as a fraction of the window size.
      const auto dx
         = static_cast<double>(x - data->prevMouseX)
         / (wndRect.right - wndRect.left);
      const auto dy
         = static_cast<double>(y - data->prevMouseY)
         / (wndRect.bottom - wndRect.top);

      // Adjust the center point to scroll the content.
      auto center = effect->GetValue<std::complex<double>>(MandelbrotParam_Center);
      center -= scale * std::complex{dx, dy};
      VALIDATE_HR(effect->SetValue(MandelbrotParam_Center, center));

      InvalidateRect(wnd, /* lpRect */ nullptr, /* bErase */ false);
   }

   data->prevMouseX = x;
   data->prevMouseY = y;
   data->isScrolling = true;
}

void MainWindow_onMouseWheel(HWND wnd,
   int /* xPos */, int /* yPos */, int zDelta, UINT /* fwKeys */)
{
   const auto data = getWindowData(wnd);

   // Adjust the scale of the effect.
   // The value is adjusted proportionally in order to maintain a constant zoom
   // speed at any scale.
   auto scale = data->mandelbrotEffect->GetValue<double>(MandelbrotParam_Scale);
   scale *= 1 - kZoomSpeed * (zDelta / WHEEL_DELTA);
   VALIDATE_HR(data->mandelbrotEffect->SetValue(MandelbrotParam_Scale, scale));

   InvalidateRect(wnd, /* lpRect */ nullptr, /* bErase */ false);
}

void MainWindow_onDestroy(HWND wnd)
{
   delete getWindowData(wnd);
   PostQuitMessage(0);
}

/*----------------------------------------------------------------------------*/

auto CALLBACK windowProc(
   HWND wnd, UINT msg,
   WPARAM wParam, LPARAM lParam) noexcept -> LRESULT
{
   switch (msg) {
      HANDLE_MSG(wnd, WM_CREATE, MainWindow_onCreate);
      HANDLE_MSG(wnd, WM_PAINT, MainWindow_onPaint);
      HANDLE_MSG(wnd, WM_SIZE, MainWindow_onSize);
      HANDLE_MSG(wnd, WM_MOUSEMOVE, MainWindow_onMouseMove);
      HANDLE_MSG(wnd, WM_MOUSEWHEEL, MainWindow_onMouseWheel);
      HANDLE_MSG(wnd, WM_DESTROY, MainWindow_onDestroy);

   default:
      return DefWindowProc(wnd, msg, wParam, lParam);
   }
}

auto createMainWindow(HINSTANCE instance) -> HWND
{
   static const auto atom = [instance] {
      auto wc = WNDCLASSEX{sizeof(WNDCLASSEX)};
      wc.style = CS_VREDRAW | CS_HREDRAW;
      wc.lpfnWndProc = windowProc;
      wc.hInstance = instance;
      wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
      wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
      wc.lpszClassName = L"Fractal_MainWindow";

      return RegisterClassEx(&wc);
   }();

   return CreateWindowEx(
      /* dwExStyle */ WS_EX_CLIENTEDGE,
      /* lpClassName */ MAKEINTATOM(atom),
      /* lpWindowName */ L"Fractal",
      /* dwStyle */ WS_OVERLAPPEDWINDOW,
      /* X */ CW_USEDEFAULT,
      /* Y */ CW_USEDEFAULT,
      /* nWidth */ CW_USEDEFAULT,
      /* nHeight */ CW_USEDEFAULT,
      /* hWndParent */ nullptr,
      /* hMenu */ nullptr,
      /* hInstance */ instance,
      /* lpParam */ nullptr);
}

} // namespace

auto APIENTRY wWinMain(
   _In_ HINSTANCE instance, _In_opt_ HINSTANCE /* prevInstance */,
   _In_ LPWSTR /* cmdLine */, _In_ int cmdShow) -> int
{
   const auto mainWindow = createMainWindow(instance);
   if (!mainWindow) {
      MessageBox(nullptr,
         L"Failed to create the main window", L"Error", MB_ICONERROR);
      return 0;
   }

   ShowWindow(mainWindow, cmdShow);
   UpdateWindow(mainWindow);

   for (MSG msg;;) {
      switch (GetMessage(&msg, nullptr, 0, 0)) {
      case -1:
         // Call failed.
         return 0;

      case 0:
         // Received WM_QUIT.
         return static_cast<int>(msg.wParam);

      default:
         TranslateMessage(&msg);
         DispatchMessage(&msg);
      }
   }
}
